//
//  main.m
//  SDK
//
//  Created by Philip on 05/18/2016.
//  Copyright (c) 2016 Philip. All rights reserved.
//

@import UIKit;
#import "HJSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HJSAppDelegate class]));
    }
}
