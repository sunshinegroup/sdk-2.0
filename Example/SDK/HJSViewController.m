//
//  HJSViewController.m
//  SDK
//
//  Created by Philip on 05/18/2016.
//  Copyright (c) 2016 Philip. All rights reserved.
//

#import "HJSViewController.h"
#import "HJSService.h"

static NSInteger lableWidth = 100;
static NSInteger textFieldWidth = 200;
static NSInteger UIHeight = 35;
static NSInteger buttonMargin = 15;
static NSInteger buttonLeftMargin = 50;

@interface HJSViewController () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listArray;
@property (nonatomic ,strong) NSArray *buttonListArray;
@property (nonatomic ,strong) NSMutableDictionary *mutableDic;
@property (nonatomic ,strong) NSMutableDictionary *dataDic;

@end

@implementation HJSViewController

- (instancetype)initWithData:(NSDictionary *)dic {
    if (self = [super init]) {
        _dataDic = [dic copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.listArray = [NSArray arrayWithObjects:@"mobile",
                                               @"authKey",
                                               @"channelKey",
                                               @"merchantId",
                                               @"ip",
                                               @"method",
                                               @"orderNO",
                                               @"consumeAmount",
                                               @"consumeRemark",
                                               @"consumeTime",
                                               @"extraField",
                                               @"syncCallbackUrl",
                                               @"asyncCallbackUrl",
                                               @"userId",
                                               @"userName",nil
                      ];
    self.buttonListArray = [NSArray arrayWithObjects:@"钱包",@"理财",@"消费",@"自定义入口",@"点击切换到QA1入口",@"版本号:", nil];
    self.mutableDic = [[NSMutableDictionary alloc] initWithDictionary:self.dataDic];
    
    self.title = @"配置页面";
    [self setupUI];
}

-(void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)style:UITableViewStylePlain];
    self.tableView.backgroundColor = [self lightGrayColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [self createFooterView];
    [self.view addSubview:self.tableView];
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(dissmissController)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *string = [self.listArray objectAtIndex:indexPath.row];
    UITextField *textField;
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        float leftMargin = (self.view.frame.size.width - (lableWidth + textFieldWidth)) / 2;
        [self createLabelText:string setFrame:CGRectMake(leftMargin, 2, lableWidth, UIHeight) addSubview:cell.contentView];
        textField = [self createTextFieldPlaceholder:string withFrame:CGRectMake(leftMargin + 110, 2, textFieldWidth, UIHeight) setTag:indexPath.row + 1 addSubview:cell.contentView];
    }
    if (!textField) {
        textField = (UITextField *)[self.view viewWithTag:indexPath.row + 1];
    }
    textField.text = [self.mutableDic valueForKey:string];
    return cell;
}

- (UIView *)createFooterView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,(self.buttonListArray.count) * (UIHeight+buttonMargin) + UIHeight)];
    NSString *string;
    for (int i = 0; i < self.buttonListArray.count; i++) {
        string = [self.buttonListArray objectAtIndex:i];
        if (i == self.buttonListArray.count-1) {
            string = [NSString stringWithFormat:@"%@%@",string,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
        }
        [self createButtonWithFrame:CGRectMake(buttonLeftMargin, (buttonMargin+UIHeight) * i + UIHeight, self.view.frame.size.width - buttonLeftMargin * 2, UIHeight) setTitle:string setTag:i+1 addSubview:view];
    }
    return view;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self.mutableDic setObject:textField.text forKey:[self.listArray objectAtIndex:textField.tag - 1]];
    return YES;
}

- (void)clickButton:(UIButton *)button {
    
    [self.view endEditing:YES];
    
    typeof (self) __weak weakSelf = self;

    if (button.tag == 1) {
        [HJSService startFlow:HJSFlowWallet withData:self.mutableDic fromController:self toCallback:^(HJSStatus status,NSDictionary *info){
            switch (status) {
                case HJSSuccess:
                    [weakSelf alertTitle:@"提示" message:[info valueForKey:@"orderId"] actionTitle:@"确定" handler:nil];
                    break;
                case HJSError:
                    [weakSelf alertTitle:@"提示" message:[info valueForKey:@"orderId"] actionTitle:@"确定" handler:nil];
                    break;
                case HJSUnknown:
                    [weakSelf alertTitle:@"提示" message:[info valueForKey:@"orderId"] actionTitle:@"确定" handler:nil];
                    break;
                default:
                    break;
            }
        }];
    }else if (button.tag == 2) {
        [HJSService startFlow:HJSFlowList withData:self.mutableDic fromController:self toCallback:nil];
    }else if (button.tag == 3) {
        [HJSService startFlow:HJSFlowPurchase withData:self.mutableDic fromController:self toCallback:^(HJSStatus status,NSDictionary *info){
            switch (status) {
                case HJSSuccess:
                    [weakSelf alertTitle:@"提示" message:[info valueForKey:@"orderId"] actionTitle:@"支付成功" handler:nil];
                    break;
                case HJSError:
                    [weakSelf alertTitle:@"提示" message:[info valueForKey:@"orderId"] actionTitle:@"支付失败" handler:nil];
                    break;
                case HJSUnknown:
                    [weakSelf alertTitle:@"提示" message:[info valueForKey:@"orderId"] actionTitle:@"未操作" handler:nil];
                    break;
                default:
                    break;
            }
        }];
    }else if (button.tag == 4) {
        [HJSService startFlow:HJSFlowCustom withData:self.mutableDic fromController:self toCallback:nil];
    }else if (button.tag == 5) {
        [self.mutableDic setValue:@"https://qa1.hfax.com" forKey:@"ip"];
    }
}

- (void)alertTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)actionTitle handler:(void(^)(NSDictionary *info))actionHaninidler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (actionHaninidler) {
            actionHaninidler(nil);
        }
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (UITextField *)createTextFieldPlaceholder:(NSString *)placeholder withFrame:(CGRect)rect setTag:(NSInteger)tag addSubview:(UIView *)view {
    UITextField *textField = [[UITextField alloc] initWithFrame:rect];
    textField.placeholder = [NSString stringWithFormat:@"请输入:%@",placeholder];
    textField.font = [UIFont systemFontOfSize:14];
    textField.tag = tag;
    textField.delegate = self;
    [view addSubview:textField];
    return textField;
}

- (void)createLabelText:(NSString *)text setFrame:(CGRect)rect addSubview:(UIView *)view {
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor blackColor];
    label.text = [NSString stringWithFormat:@"%@:",text];
    label.textAlignment = NSTextAlignmentRight;
    [view addSubview:label];
}

- (void)createButtonWithFrame:(CGRect)rect setTitle:(NSString *)title setTag:(NSInteger)tag addSubview:(UIView *)view {
    UIButton *button = [[UIButton alloc]initWithFrame:rect];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [self orangeColor];
    button.layer.cornerRadius = 5;
    button.tag = tag;
    [view addSubview:button];
}

- (UIColor *)orangeColor {
    return [self stringToColor:@"0xf26d4f"];
}

- (UIColor *)lightGrayColor {
    return [self stringToColor:@"0xf1f1f1"];
}

- (UIColor *)stringToColor:(NSString *)string {
    string = [string stringByReplacingOccurrencesOfString:@"0x" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"#" withString:@""];
    
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 0;
    [[NSScanner scannerWithString:[string substringWithRange:range]] scanHexInt:&red];
    range.location = 2;
    [[NSScanner scannerWithString:[string substringWithRange:range]] scanHexInt:&green];
    range.location = 4;
    [[NSScanner scannerWithString:[string substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
}

- (void)dissmissController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
