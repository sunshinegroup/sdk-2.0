//
//  HJSViewController.h
//  SDK
//
//  Created by Philip on 05/18/2016.
//  Copyright (c) 2016 Philip. All rights reserved.
//

@import UIKit;

@interface HJSViewController : UIViewController

- (instancetype)initWithData:(NSDictionary *)dic;

@end