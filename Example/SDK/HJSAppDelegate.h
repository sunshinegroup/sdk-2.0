//
//  HJSAppDelegate.h
//  SDK
//
//  Created by Philip on 05/18/2016.
//  Copyright (c) 2016 Philip. All rights reserved.
//

@import UIKit;

@interface HJSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
