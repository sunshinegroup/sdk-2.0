//
//  LoginViewController.m
//  SDK
//
//  Created by 黄瑞东 on 16/9/27.
//  Copyright © 2016年 Philip. All rights reserved.
//

#import "LoginViewController.h"
#import "HJSViewController.h"

#define View_Height self.view.frame.size.height
#define View_Width self.view.frame.size.width
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

static NSInteger leftMargin = 20;
static CGFloat buttonWidth = 80.0f;
static CGFloat buttonHeight = 35.0f;
static CGFloat marginTop = 20.0f;
static NSInteger buttonCounts = 4; //每行显示的按钮数量
static NSInteger imageWidth = 80;
static NSInteger channelButtonTag = 10000;
static NSInteger requestTime = 10;

#ifdef PREPRODUCTION
static NSString *baseUrl = @"http://172.16.2.34:15052/paysimulator/sdk/";
#else
static NSString *baseUrl = @"https://10.10.127.214:18666/paysimulator/sdk/";
#endif

@interface LoginViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *listArray;
@property (nonatomic ,strong) NSMutableDictionary *mutableDic;
@property (nonatomic ,strong) NSMutableDictionary *dataDic;
@property (nonatomic ,strong) NSArray *channelArray;
@property (nonatomic ,strong) UIVisualEffectView *blurView;
@property (nonatomic ,assign) NSInteger channelId;
@property (nonatomic ,assign) BOOL isRequest;
@property (nonatomic ,assign) float lastButtonMaxY;
@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.channelId = 0;
    [self refreshChannelAPI];
}

- (void)refreshChannelAPI {
    self.isRequest = NO;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrl,@"getChannelKey"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:requestTime];
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    [connection start];
    self.blurView.alpha = 1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listArray = [NSArray arrayWithObjects:@"mobile",@"password",@"url", nil];
    self.mutableDic = [NSMutableDictionary dictionary];
    self.title = @"登录页面";
    
    BOOL isDefaultData = [self readNSUserDefaults];
    
    if (!isDefaultData) {
        [self defaultData];
    }
    
    [self setupUI];
}

- (void)defaultData {
    for (int i = 0; i < self.listArray.count; i++) {
        
        switch (i) {
            case 0:
                [self.mutableDic setValue:@"15512345677" forKey:[self.listArray objectAtIndex:i]];
                break;
            case 1:
                [self.mutableDic setValue:@"123456" forKey:[self.listArray objectAtIndex:i]];
                break;
            case 2:
                [self.mutableDic setValue:baseUrl forKey:[self.listArray objectAtIndex:i]];
                break;
            default:
                break;
        }
    }
}

- (void)getRequestData {
    NSString *mobileString = [self.mutableDic valueForKey:[self.listArray objectAtIndex:0]];
    NSString *baseURLString = [NSString stringWithFormat:@"%@getUserInfoById",[self.mutableDic valueForKey:[self.listArray objectAtIndex:2]]];
    NSString *channelKeyString = [[self.channelArray objectAtIndex:self.channelId - 1] valueForKey:@"channelNum"];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?mobile=%@&channelKey=%@",baseURLString,mobileString,channelKeyString]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:requestTime];
    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    [connection start];
    self.blurView.alpha = 1;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    if (self.isRequest) {
        self.dataDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }else{
        self.channelArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    self.blurView.alpha = 0;
    if (!self.isRequest) {
        self.isRequest = YES;
        self.tableView.tableFooterView = [self createFooterView];
    }else{
        if (self.dataDic) {
            HJSViewController *hjsVC = [[HJSViewController alloc] initWithData:self.dataDic];
            
            UITabBarController *tab = [UITabBarController new];
            UINavigationController *navigationControllerOne = [[UINavigationController alloc] initWithRootViewController:hjsVC];
            [tab addChildViewController:navigationControllerOne];
            
            UINavigationController *navigationControllerTwo = [[UINavigationController alloc] initWithRootViewController:[UIViewController new]];
            [tab addChildViewController:navigationControllerTwo];
            navigationControllerTwo.view.backgroundColor = [UIColor whiteColor];
            navigationControllerTwo.title = @"测试界面";
            
            [self presentViewController:tab animated:YES completion:nil];
            self.dataDic = nil;
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    self.blurView.alpha = 0;
    [self alertTitle:@"提示" message:@"网络不给力,请检查连接的网络是否为内网!" actionTitle:@"确定" handler:nil];
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount]== 0) {
        NSURLCredential* cre = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        [challenge.sender useCredential:cre forAuthenticationChallenge:challenge];
    }
}

- (void)alertTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)actionTitle handler:(void(^)(NSDictionary *info))actionHaninidler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)style:UITableViewStylePlain];
    self.tableView.backgroundColor = [self lightGrayColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [self createFooterView];
    self.tableView.tableHeaderView = [self createHeaderView];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    self.blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    self.blurView.frame = self.view.bounds;
    [self.view addSubview:self.blurView];
    self.blurView.alpha = 0;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self.mutableDic setObject:textField.text forKey:[self.listArray objectAtIndex:textField.tag - 1]];
    return YES;
}

-(BOOL)readNSUserDefaults {
    BOOL isDefaultData = NO;
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    
    for (int i = 0; i < self.listArray.count; i++) {
        NSString *string = [userDefaultes objectForKey:[self.listArray objectAtIndex:i]];
        if (string.length) {
            [self.mutableDic setObject:[userDefaultes objectForKey:[self.listArray objectAtIndex:i]] forKey:[self.listArray objectAtIndex:i]];
            isDefaultData = YES;
        }
    }
    return isDefaultData;
}

-(void)saveNSUserDefaults {
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    
    for (int i = 0; i < self.listArray.count; i++) {
        [userDefaultes setObject:[self.mutableDic valueForKey:[self.listArray objectAtIndex:i]] forKey:[self.listArray objectAtIndex:i]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSString *string = [self.listArray objectAtIndex:indexPath.row];
    UITextField *textField;
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        textField = [self createTextFieldPlaceholder:string withFrame:CGRectMake(leftMargin, 2, View_Width - leftMargin * 2, 40) setTag:indexPath.row + 1 addSubview:cell.contentView];
    }
    if (!textField) {
        textField = (UITextField *)[self.view viewWithTag:indexPath.row + 1];
    }
    textField.text = [self.mutableDic valueForKey:string];
    return cell;
}

- (UIView *)createFooterView {
    
    UIView *view = [UIView new];

    CGFloat marginX = (View_Width - buttonWidth * buttonCounts) / (buttonCounts+1);
    
    if (self.channelArray.count > 0) {
        for (int i = 0; i < self.channelArray.count; i++) {
            // 每个按钮的行索引
            int hang = i / buttonCounts;
            // 当前按钮的列索引
            int lie = i % buttonCounts;
            
            CGFloat buttonX = marginX + lie * (buttonWidth + marginX);
            CGFloat buttonY = marginTop + hang * (buttonHeight + marginTop);
            
            [self createButtonWithFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight) setTitle:[[self.channelArray objectAtIndex:i] valueForKey:@"channelName"] setTag:channelButtonTag + i addSubview:view bgColor:[UIColor lightGrayColor]];
            if (i == self.channelArray.count - 1) {
                self.lastButtonMaxY = buttonY + buttonHeight;
            }
        }
    }
    [self createButtonWithFrame:CGRectMake(marginX, self.lastButtonMaxY + marginTop, self.view.frame.size.width - marginX * 2, buttonHeight) setTitle:@"登录" setTag:1 addSubview:view bgColor:[self blueColor]];
    [self createButtonWithFrame:CGRectMake(marginX, self.lastButtonMaxY + marginTop * 2 + buttonHeight, self.view.frame.size.width - marginX * 2, buttonHeight) setTitle:@"直接进入" setTag:2 addSubview:view bgColor:[self blueColor]];
    
    view.frame = CGRectMake(0, 0, View_Width, self.lastButtonMaxY + marginTop * 2 + buttonHeight * 2);
    return view;
}

- (UIView *)createHeaderView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70 + imageWidth)];
    UIButton *button = [self createButtonWithFrame:CGRectMake(View_Width/2-imageWidth/2, 40, imageWidth, imageWidth) setTitle:nil setTag:9999 addSubview:view bgColor:[UIColor darkGrayColor]];
    button.layer.cornerRadius = 5.0;
    
    UIButton *cleanButton = [self createButtonWithFrame:CGRectMake(View_Width - 90, 30, 80, 30) setTitle:@"清除数据" setTag:8888 addSubview:view bgColor:nil];
    [cleanButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    
    return view;
}

- (BOOL)isError{
    for (int i = 0; i < self.listArray.count; i++) {
        if (![self.mutableDic valueForKey:[self.listArray objectAtIndex:i]] ||
            [[self.mutableDic valueForKey:[self.listArray objectAtIndex:i]] isEqualToString:@""]) {
            [self alertTitle:nil message:[NSString stringWithFormat:@"%@ 为空。",[self.listArray objectAtIndex:i]] actionTitle:@"确定" handler:nil];
            return NO;
        }
    }
    
    if (self.channelId == 0) {
        [self alertTitle:nil message:@"请选择渠道" actionTitle:@"确定" handler:nil];
        return NO;
    }
    return YES;
}

- (void)clickButton:(UIButton *)button{
    
    [self.view endEditing:YES];
    [self saveNSUserDefaults];
    
    if (button.tag == 1) {
        BOOL isError = [self isError];
        if (!isError) {
            return;
        }
        [self getRequestData];
    }else if (button.tag == 9999){
        [self refreshChannelAPI];
    }else if (button.tag == 2){
        HJSViewController *hjsVC = [[HJSViewController alloc] initWithData:nil];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:hjsVC] animated:YES completion:nil];
    }else if (button.tag == 8888) {
        [self.mutableDic removeAllObjects];
        [self.tableView reloadData];
    }else {
        
        for (int i = 0; i < self.channelArray.count; i++) {
            UIButton *bt = (UIButton *)[self.view viewWithTag:channelButtonTag + i];
            if (button.tag == bt.tag) {
                self.channelId = i + 1;
                bt.backgroundColor = [self blueColor];
            }else{
                bt.backgroundColor = [UIColor lightGrayColor];
            }
        }
    }
}

- (UIButton *)createButtonWithFrame:(CGRect)rect setTitle:(NSString *)title setTag:(NSInteger)tag addSubview:(UIView *)view bgColor:(UIColor *)bgColor{
    UIButton *button = [[UIButton alloc]initWithFrame:rect];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = bgColor;
    button.tag = tag;
    [view addSubview:button];
    return button;
}

- (UITextField *)createTextFieldPlaceholder:(NSString *)placeholder withFrame:(CGRect)rect setTag:(NSInteger)tag addSubview:(UIView *)view {
    UITextField *textField = [[UITextField alloc] initWithFrame:rect];
    textField.placeholder = placeholder;
    textField.font = [UIFont systemFontOfSize:14];
    textField.tag = tag;
    textField.delegate = self;
    textField.textAlignment = NSTextAlignmentCenter;
    [view addSubview:textField];
    return textField;
}

- (UIColor *)lightGrayColor {
    return UIColorFromRGB(0xf1f1f1);
}

- (UIColor *)blueColor {
    return  UIColorFromRGB(0x00aeef);
}

@end
