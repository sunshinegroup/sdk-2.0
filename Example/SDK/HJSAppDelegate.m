//
//  HJSAppDelegate.m
//  SDK
//
//  Created by Philip on 05/18/2016.
//  Copyright (c) 2016 Philip. All rights reserved.
//

#import "HJSAppDelegate.h"
#import "HJSViewController.h"
#import "LoginViewController.h"
#import <PHWebViewController.h>
#import "WXApi.h"

#ifdef PREPRODUCTION
static NSString *kWeChatKey = @"wxb39789d27bd18fff";
#else
static NSString *kWeChatKey = @"wx0bff059d41af1e8c";
#endif



@implementation HJSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

#ifdef UITEST
    NSURL *accessUrl = [NSURL URLWithString:[[NSBundle mainBundle]pathForResource:@"index.html" ofType:nil]];
    PHWebViewController *loginVC = [[PHWebViewController alloc] initWithParameterData:nil url:accessUrl withStatusHander:nil lastPushCallback:nil popToEntryCallback:nil lastPresentCallback:nil isEntry:nil entryController:nil fromController:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [[UINavigationController alloc]initWithRootViewController:loginVC];
    [self.window makeKeyAndVisible];
#else
    LoginViewController *loginVC = [LoginViewController new];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = loginVC;
    [self.window makeKeyAndVisible];
#endif
    
    [WXApi registerApp:kWeChatKey];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
