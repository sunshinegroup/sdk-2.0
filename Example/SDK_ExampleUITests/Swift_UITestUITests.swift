//
//  Swift_UITestUITests.swift
//  Swift_UITestUITests
//
//  Created by 邹彦军 on 16/12/14.
//  Copyright © 2016年 邹彦军. All rights reserved.
//

import XCTest

class Swift_UITestUITests: XCTestCase {

    let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    //充值页面
    func testTop_up() {//钱包页面 -->充值页面 -->实名认证流程 -->绑定银行卡流程 -->设置交易密码流程 -->确定（输入完充值金额） -->继续投资and立即理财
        self.tapElement(elementName: "充值（present）")
        self.lookingElement(elementName: "充值页面")
        self.tapElement(elementName: "实名认证（present）")
        self.lookingElement(elementName: "实名认证页面")
        self.tapElement(elementName: "去绑定银行卡（present）")
        self.lookingElement(elementName: "绑定银行卡页面")
        self.tapElement(elementName: "设置交易密码（present）")
        self.lookingElement(elementName: "设置交易密码页面")
        self.tapElement(elementName: "完成（close）")
        self.lookingElement(elementName: "充值页面")
        self.tapElement(elementName: "确定//输入交易密码（push）")
        self.lookingElement(elementName: "充值成功页面")
        self.tapElement(elementName: "继续充值（push）")
        self.lookingElement(elementName: "充值页面")
        self.tapElement(elementName: "确定//输入交易密码（push）")
        self.lookingElement(elementName: "充值成功页面")
        self.tapElement(elementName: "立即理财（popToEntry）")
        self.lookingElement(elementName: "理财列表页面")
        self.tapElement(elementName: "详情页面1（push）")
    }
    
    //列表页面
    func testList() {//钱包页面 -->投资理财页面 -->项目介绍页面 -->确认投资页面 -->投资结果页面 -->查看已投项目 -->投资结果页面 -->项目介绍页面 -->确认投资页面 -->投资结果页面 -->理财列表页面
        self.tapElement(elementName: "投资理财（push）")
        self.lookingElement(elementName: "理财列表页面")
        self.tapElement(elementName: "详情页面1（push）")
        self.lookingElement(elementName: "项目介绍页面")
        self.shareFunction()
        self.tapElement(elementName: "追加投资（push）")
        self.lookingElement(elementName: "确认投资页面")
        self.tapElement(elementName: "确认投资（push）")
        self.lookingElement(elementName: "投资结果页面")
        self.tapElement(elementName: "查看已投项目（popToEntry）")
        self.lookingElement(elementName: "投资明细页面")
        self.tapElement(elementName: "追加投资（push）")
        self.lookingElement(elementName: "项目介绍页面")
        self.tapElement(elementName: "追加投资（push）")
        self.lookingElement(elementName: "确认投资页面")
        self.tapElement(elementName: "确认投资（push）")
        self.lookingElement(elementName: "投资结果页面")
        self.tapElement(elementName: "继续理财（popToEntry）")
        self.lookingElement(elementName: "理财列表页面")
    }
    
    //帮助中心
    func testHelpCenter() {
        
        self.tapElement(elementName: "帮助中心（push）")
        self.lookingElement(elementName: "帮助中心页面")
        self.tapElement(elementName: "打电话（callPhone）")
        self.lookingElement(elementName: "提示")
        self.lookingElement(elementName: "是否拨打客服电话")
        XCTAssert(app.buttons["呼叫"].exists, "找不到呼叫")
        XCTAssert(app.buttons["取消"].exists, "找不到取消")
        let callphoneStaticText = app.staticTexts["打电话（callPhone）"]
        let collectionViewsQuery = app.alerts["提示"].collectionViews
        collectionViewsQuery.buttons["取消"].tap()
        callphoneStaticText.tap()
        collectionViewsQuery.buttons["呼叫"].tap()
        
        if UIApplication.shared.canOpenURL(NSURL(string :"tel://1008611") as! URL) {
            UIApplication.shared.openURL(NSURL(string :"tel://15974462468")! as URL)
        } else {
            self.lookingElement(elementName: "当前设备不支持打电话")
            app.alerts.collectionViews.buttons["确定"].tap()
        }
    }
    
    //测试push、pop && present、close
    func testPushPop() {
        for _ in 1...5 {
            app.staticTexts["投资理财（push）"].tap()
            app.staticTexts["返回（pop）"].tap()
        }
        for _ in 1...5 {
            app.staticTexts["充值（present）"].tap()
            app.staticTexts["返回（close）"].tap()
        }
    }
    
    //分享功能
    func shareFunction() {
        self.tapElement(elementName: "分享（share）")
        self.lookingElement(elementName: "微信")
        self.lookingElement(elementName: "朋友圈")
        self.lookingElement(elementName: "短信")
        self.app.tap()
    }
    
    //点击某元素
    func tapElement(elementName:String) {
        self.lookingElement(elementName: elementName)
        app.staticTexts[elementName].tap()
    }
    
    //查看某元素
    func lookingElement(elementName:String) {
        Thread.sleep(forTimeInterval: 0.25)
        XCTAssert(app.staticTexts[elementName].exists, "找不到\(elementName)")
    }
}
