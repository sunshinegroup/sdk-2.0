//
//  HJSService.m
//  Pods
//
//  Created by 邹彦军 on 16/7/13.
//
//

#import "WXApi.h"
#import "HJSService.h"
#import "PHWebViewController.h"

static NSString *authEntryUrl = @"/nextgen/n/authEntry";
static NSString *listUrl      = @"/nextgen/n/filtration";
static NSString *purchaseUrl  = @"/nextgen/n/consumEntry";

#ifdef SANDBOX
static NSString *baseUrl      = @"https://qa5.hfax.com";
#elif DEBUG
static NSString *baseUrl      = @"https://qa5.hfax.com";
#elif PREPRODUCTION
static NSString *baseUrl      = @"https://prehfax.hfax.com";
#else
static NSString *baseUrl      = @"https://www.hfax.com";
#endif


@implementation HJSService
+ (void)startFlow:(HJSFlowType)flow withData:(NSDictionary *)data fromController:(UIViewController *)fromController toCallback:(void(^)(HJSStatus status,NSDictionary *info))resultHandler{
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    NSString *baseUrlString = [data valueForKey:@"ip"] ? : baseUrl;
    
    NSString *methodUrlString = [data valueForKey:@"method"];
    if (![methodUrlString hasPrefix:@"/"]) {
        methodUrlString = [NSString stringWithFormat:@"/%@",methodUrlString];
    }
    
    switch (flow) {
        case HJSFlowWallet:{
            NSURL *accessUrl = [self loadBaseUrl:baseUrlString endPoint:authEntryUrl withParam:data];
            
            PHWebViewController * webViewController = [[PHWebViewController alloc] initWithParameterData:data url:accessUrl withStatusHander:nil lastPushCallback:nil popToEntryCallback:nil lastPresentCallback:nil isEntry:YES entryController:nil fromController:fromController withWebViewConfiguration:nil];
            webViewController.showProgressBar = YES;
            webViewController.hidesBottomBarWhenPushed = YES;
            [fromController.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case HJSFlowList:{
            NSURL *accessUrl = [self loadBaseUrl:baseUrlString endPoint:listUrl withParam:data];
            
            PHWebViewController * webViewController = [[PHWebViewController alloc] initWithParameterData:data url:accessUrl withStatusHander:nil lastPushCallback:nil popToEntryCallback:nil lastPresentCallback:nil isEntry:YES entryController:nil fromController:fromController withWebViewConfiguration:nil];
            
            webViewController.hidesBottomBarWhenPushed = YES;
            [fromController.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case HJSFlowPurchase:{
            NSURL *accessUrl = [self loadBaseUrl:baseUrlString endPoint:purchaseUrl withParam:data];
            
            PHWebViewController * webViewController = [[PHWebViewController alloc] initWithParameterData:data url:accessUrl withStatusHander:^(HJSStatus status,NSDictionary *info){
                [fromController.navigationController popToViewController:fromController animated:YES];
                if(resultHandler){
                    resultHandler(status,info);
                }
            } lastPushCallback:nil popToEntryCallback:nil lastPresentCallback:nil isEntry:YES entryController:nil fromController:fromController withWebViewConfiguration:nil];
            
            webViewController.hidesBottomBarWhenPushed = YES;
            [fromController.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case HJSFlowCustom:{
            NSURL *accessUrl = [self loadBaseUrl:baseUrlString endPoint:methodUrlString withParam:data];
            
            PHWebViewController * webViewController = [[PHWebViewController alloc] initWithParameterData:data url:accessUrl withStatusHander:nil lastPushCallback:nil popToEntryCallback:nil lastPresentCallback:nil isEntry:YES entryController:nil fromController:fromController withWebViewConfiguration:nil];
            
            webViewController.hidesBottomBarWhenPushed = YES;
            [fromController.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        default:{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"入口参数错误" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:action];
            [fromController presentViewController:alertController animated:YES completion:nil];
        }
    }
}


+ (NSURL *)loadBaseUrl:(NSString *)baseUrlString endPoint:(NSString *)endPoint withParam:(NSDictionary *)param{
    NSMutableString* string = [NSMutableString stringWithString:[baseUrlString stringByAppendingString:endPoint]];
    [string appendString:@"?"];
    for (NSString* key in param) {
        NSString* value = [NSString stringWithFormat:@"%@", [param objectForKey:key]];
        value = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedValue = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,
                                                                                                      (CFStringRef)value, nil,
                                                                                                      (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
        if (key && ![value isEqual:NSNull.null]) {
            [string appendString:[NSString stringWithFormat:@"%@=%@&", key, encodedValue]];
        } else {
            [string appendString:[NSString stringWithFormat:@"%@&", key]];
        }
    }
    
    [string appendString:[NSString stringWithFormat:@"version=%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]]];
    
    return [NSURL URLWithString:string];
}

+ (void)registerWeChatID:(NSString *)IdNum{
    [WXApi registerApp:IdNum];
}

@end
