//
//  HJSService.h
//  所有Api接口
//
//  Created by HFaxSDK on 16-7-14.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,HJSFlowType){
    HJSFlowWallet,          /**< 进入钱包        */
    HJSFlowList,            /**< 进入理财列表     */
    HJSFlowPurchase,        /**< 进入购买流程     */
    HJSFlowCustom = 99999   /**< 预留接口，目前不用*/
};



typedef NS_ENUM(NSInteger,HJSStatus){
    HJSSuccess,             /**< 购买成功    */
    HJSError,               /**< 购买异常    */
    HJSUnknown              /**< 未知原因    */
};


/*! @brief 惠金所Api接口函数类
 *
 * 该类封装了惠金所SDK的所有接口
 */
@interface HJSService : NSObject

/**
 *  SDK的入口方法
 *
 *  @param flow           要发起流程的类型
 *  @param data           @{ @"authKey",           // SDK用户key         （非必填）
                             @"userName",          // 第三方平台用户名     （非必填）
                             @"channelKey",        // 渠道key            （所有请求必填）
                             @"mobile",            // 用户手机号          （所有请求必填）
                             @"merchantId",        // 商户号             （所有请求必填）
                             @"userId",            // 三方平台用户ID      （所有请求必填）
                             @"orderNO",           // 订单号             （消费时必填）
                             @"consumeAmount",     // 消费金额           （消费时必填）
                             @"consumeRemark",     // 消费备注           （消费时必填）
                             @"consumeTime",       // 消费时间           （消费时必填）
                             @"extraField",        // 消费回调扩展字段    （非必填）
                             @"syncCallbackUrl",   // 消费前台回调地址    （消费时必填）
                             @"asyncCallbackUrl",  // 消费结果后台回调地址 （消费时必填）
                           }

 *  @param fromController 来源控制器,流程结束后会返回到来源控制器
 *  @param resultHandler  返回购买结果的回调,包含三种状态及详情信息
 */
+ (void)startFlow:(HJSFlowType)flow withData:(NSDictionary *)data fromController:(UIViewController *)fromController toCallback:(void(^)(HJSStatus status,NSDictionary *info))resultHandler;


/**
 *  注册微信分享必要的key
 *
 *  @param IdNum 注册'微信开放平台'获取的key值
 */
+ (void)registerWeChatID:(NSString *)IdNum;

@end

