//
//  PHShareViewController.h
//  share
//
//  Created by 1234 on 16/7/1.
//  Copyright © 2016年. All rights reserved.
//

#import <UIKit/UIKit.h>
extern NSString * const kShareHdcodeTag;
extern NSString * const PHNotificationSendShareCodeObserver;
@interface PHShareViewController : UIViewController

- (instancetype)initWithContentString:(NSString *)content DismissHandler:(void(^)(PHShareViewController *controller))dismissHandler;

- (instancetype)initWithShareTitleString:(NSString *)shareTitleString shareContentString:(NSString *)shareContentString shareSMSString:(NSString *)shareSMSString shareFriendString:(NSString *)shareFriendString shareUrlString:(NSString *)shareUrlString dismissHandler:(void(^)(PHShareViewController *controller))dismissHandler;
@end
