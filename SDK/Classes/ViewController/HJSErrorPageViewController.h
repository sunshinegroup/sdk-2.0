//
//  HJSErrorPageViewController.h
//  Pods
//
//  Created by 黄瑞东 on 17/2/13.
//
//

#import <UIKit/UIKit.h>

@interface HJSErrorPageViewController : UIViewController

@property (nonatomic, copy) void (^successHandler)(HJSErrorPageViewController *errorPageViewController);

@end
