//
//  HJSErrorPageViewController.m
//  Pods
//
//  Created by 黄瑞东 on 17/2/13.
//
//

#import "HJSErrorPageViewController.h"
#import "HJSToolClass.h"

static CGFloat imageViewWidth = 60;
static CGFloat imageViewY = 100;
static CGFloat labelMargin = 40;

@interface HJSErrorPageViewController ()

@property (strong , nonatomic) UIImageView *imageView;
@property (strong , nonatomic) UILabel *titleLabel;
@property (strong , nonatomic) UILabel *contentLabel;

@end

@implementation HJSErrorPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF1F1F1);
    self.title = @"出错啦";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xF13D3D);
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    UIBarButtonItem*backItem= [[UIBarButtonItem alloc]initWithImage:[self imagesNamedFromCustomBundle:@"icon_backButton.png"] style:UIBarButtonItemStyleDone target:self action:@selector(backVC)];
    self.navigationItem.leftBarButtonItem = backItem;
    
    CGFloat imageViewX = self.view.frame.size.width/2 - imageViewWidth/2;
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, imageViewWidth, imageViewWidth)];
    self.imageView.image = [self imagesNamedFromCustomBundle:@"icon_LinkError.png"];
    [self.view addSubview:self.imageView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelMargin, imageViewY + imageViewWidth + 10, self.view.frame.size.width - labelMargin * 2, 40)];
    self.titleLabel.text = @"网络异常";
    self.titleLabel.font = [UIFont systemFontOfSize:18];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.titleLabel];
    
    self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelMargin, imageViewY + imageViewWidth + 40, self.view.frame.size.width - labelMargin * 2, 40)];
    self.contentLabel.text = @"网络不给力，请检查网络。";
    self.contentLabel.font = [UIFont systemFontOfSize:14];
    self.contentLabel.textColor = UIColorFromRGB(0x999999);
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.contentLabel];
}

- (void)backVC {
    if (self.successHandler) {
        self.successHandler(self);
    }
}

- (UIImage *)imagesNamedFromCustomBundle:(NSString *)imgName {
    NSString *bundlePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"HFaxSDK.bundle"];
    if (bundlePath) {
        NSString *img_path = [bundlePath stringByAppendingPathComponent:imgName];
        return [UIImage imageWithContentsOfFile:img_path];
    } else {
        return nil;
    }
}

@end
