//
//  PHShareViewController.m
//  share
//
//  Created by 1234 on 16/7/1.
//  Copyright © 2016年. All rights reserved.
//

#import "PHShareViewController.h"
#import <MessageUI/MFMessageComposeViewController.h>
#import "WXApi.h"
#import "WXApiObject.h"

NSString* const kShareHdcodeTag = @"hdcode";
NSString * const PHNotificationSendShareCodeObserver = @"PHNotificationSendShareCodeObserver";
static NSInteger const shareViewHeight = 150;
static NSInteger const iconMargin = 30;
static NSInteger const iconWidth = 80;
static const NSString* kShareTitleTag = @"title";
static const NSString* kShareContentTag = @"content";
static const NSString* kShareURLTag = @"url";
static const NSString* kShareSMSTag = @"smscontent";
static const NSString* kShareFriendTag = @"friend";
static const NSString* kShareFriendHide = @"friendHide";


@interface PHShareViewController ()<MFMessageComposeViewControllerDelegate>

@property (nonatomic, copy) void(^dismissHandler)(PHShareViewController *);
@property (nonatomic, strong) UIVisualEffectView *blurView;
@property (nonatomic, strong) UIToolbar *toolbar;  // 适配iOS 7毛玻璃的效果
@property (nonatomic, strong) UIView *shareView;
@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIButton *WeChatShareButton;
@property (nonatomic, strong) UIButton *friendsShareButton;
@property (nonatomic, strong) UIButton *SMSShareButton;
@property (nonatomic, copy) NSString *shareTitleString;
@property (nonatomic, copy) NSString *shareContentString;
@property (nonatomic, copy) NSString *shareFriendString;
@property (nonatomic, copy) NSString *shareUrlString;
@property (nonatomic, copy) NSString *shareSMSString;
@property (nonatomic, copy) NSString *shareFriendHide;
@property (nonatomic, copy) NSString *shareHdcodeString;

@end

@implementation PHShareViewController

- (instancetype)initWithContentString:(NSString *)content DismissHandler:(void(^)(PHShareViewController *controller))dismissHandler{
    if (self = [super init]) {
        
        _dismissHandler = dismissHandler;
        
        NSURLComponents* fakeURL = [NSURLComponents componentsWithString:content];
        
        for (int i = 0; i < fakeURL.queryItems.count; i++) {
            NSURLQueryItem *obj = [fakeURL.queryItems objectAtIndex:i];
            
            if ([obj.name isEqual:kShareURLTag]) {
                _shareUrlString = [obj.value copy];
            }
            
            if ([obj.name isEqual:kShareTitleTag]) {
                _shareTitleString = [obj.value copy];
            }
            
            if ([obj.name isEqual:kShareContentTag]) {
                _shareContentString = [obj.value copy];
            }
            
            if ([obj.name isEqual:kShareFriendTag]) {
                _shareFriendString = [obj.value copy];
            }
            
            if ([obj.name isEqual:kShareSMSTag]) {
                _shareSMSString = [obj.value copy];
            }
            
            if ([obj.name isEqual:kShareFriendHide]) {
                _shareFriendHide = [obj.value copy];
            }
            
            if ([obj.name isEqual:kShareHdcodeTag]) {
                [[NSUserDefaults standardUserDefaults] setObject:[obj.value copy] forKey:kShareHdcodeTag];
            }
        }
    }
    return self;
}

- (instancetype)initWithShareTitleString:(NSString *)shareTitleString shareContentString:(NSString *)shareContentString shareSMSString:(NSString *)shareSMSString shareFriendString:(NSString *)shareFriendString shareUrlString:(NSString *)shareUrlString dismissHandler:(void(^)(PHShareViewController *controller))dismissHandler {
    if (self = [super init]) {
        _dismissHandler = dismissHandler;
        _shareUrlString = [shareUrlString copy];
        _shareSMSString = [shareSMSString copy];
        _shareTitleString = [shareTitleString copy];
        _shareFriendString = [shareFriendString copy];
        _shareContentString = [shareContentString copy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (NSClassFromString(@"UIBlurEffect")) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        self.blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        self.blurView.frame = self.view.bounds;
        [self.view addSubview:self.blurView];
    } else {
        UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:self.view.bounds];
        toolbar.barStyle = UIBarStyleDefault;
        [self.view addSubview:toolbar];
    }
    
    self.shareView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, shareViewHeight)];
    self.shareView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.shareView];
    
    [self initShareView];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    typeof(self) __weak weakSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        weakSelf.shareView.frame = CGRectMake(0, weakSelf.view.frame.size.height - shareViewHeight, weakSelf.view.frame.size.width, shareViewHeight);
    }];
}

- (void)initShareView{
    self.shareButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.shareView.frame.size.width, 30)];
    [self.shareButton setTitle:@"分享" forState:UIControlStateNormal];
    [self.shareButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.shareButton setBackgroundColor:[UIColor whiteColor]];
    [self.shareButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.shareView addSubview:self.shareButton];
    [self.shareButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    if ([self.shareFriendHide isEqualToString:@"1"]) {
        self.WeChatShareButton = [[UIButton alloc]initWithFrame:CGRectMake(iconMargin * 2,self.shareButton.frame.size.height,iconWidth, iconWidth)];
        self.SMSShareButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - iconMargin * 2 - iconWidth, self.WeChatShareButton.frame.origin.y, iconWidth, iconWidth)];
    }else{
        self.WeChatShareButton = [[UIButton alloc]initWithFrame:CGRectMake(iconMargin,self.shareButton.frame.size.height,iconWidth, iconWidth)];
        self.friendsShareButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - iconWidth/2, self.WeChatShareButton.frame.origin.y, iconWidth, iconWidth)];
        self.SMSShareButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - iconMargin - iconWidth, self.WeChatShareButton.frame.origin.y, iconWidth, iconWidth)];
    }
    
    [self setUpTheButton:self.WeChatShareButton imageName:@"icon_share_wechat" withTag:1 labelText:@"微信"];
    [self setUpTheButton:self.friendsShareButton imageName:@"icon_share_circle" withTag:2 labelText:@"朋友圈"];
    [self setUpTheButton:self.SMSShareButton imageName:@"icon_share_sms" withTag:3 labelText:@"短信"];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self dismiss];
}

- (void)dismiss{
    typeof(self) __weak weakSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        weakSelf.shareView.frame = CGRectMake(0, weakSelf.view.frame.size.height, weakSelf.view.frame.size.width, shareViewHeight);
        if (NSClassFromString(@"UIBlurEffect")) {
            weakSelf.blurView.alpha = 0;
        } else {
            weakSelf.toolbar.alpha = 0;
        }
    } completion:^(BOOL finished) {
        if (self.dismissHandler) {
            self.dismissHandler(self);
        }
    }];
}

-(void)setUpTheButton:(UIButton *)button imageName:(NSString *)name withTag:(NSInteger)tag labelText:(NSString *)labelText{
    if (!button) return;
    button.showsTouchWhenHighlighted = YES;
    button.tag = tag;
    [button setImage:[self imagesNamedFromCustomBundle:name] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    [self.shareView addSubview:button];
    [self setUpTheLabelFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + iconWidth, iconWidth, 20) title:labelText];
}

-(void)setUpTheLabelFrame:(CGRect)frame title:(NSString *)string{
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor darkGrayColor];
    label.text = string;
    label.textAlignment = NSTextAlignmentCenter;
    [self.shareView addSubview:label];
}

-(void)share:(UIButton *)button{
    if (button.tag == 1) {
        [self shareWeChat];
    }else if (button.tag == 2){
        [self shareFriends];
    }else if (button.tag == 3){
        [self shareSMS];
    }
}

- (void)shareWeChat{
    if ([WXApi isWXAppInstalled] == YES) {
        [self sendLinkContent:self.shareTitleString
              withDescription:self.shareContentString
                    withImage:@"icon_share"
                      withUrl:self.shareUrlString
                        scene:0];
        [self dismiss];
    } else {
        [self alertTitle:@"提示" message:@"您的设备上没有安装微信。" actionTitle:@"确定"];
    }
}

- (void)shareFriends{
    if ([WXApi isWXAppInstalled] == YES) {
        
        if(nil != self.shareFriendString){
            [self sendLinkContent:self.shareFriendString
                  withDescription:self.shareContentString
                        withImage:@"icon_share"
                          withUrl:self.shareUrlString
                            scene:1];
            [self dismiss];
        }else{
            [self sendLinkContent:self.shareTitleString
                  withDescription:self.shareContentString
                        withImage:@"icon_share"
                          withUrl:self.shareUrlString
                            scene:1];
            [self dismiss];
        }
    } else {
        [self alertTitle:@"提示" message:@"您的设备上没有安装微信。" actionTitle:@"确定"];
    }
}

- (void)shareSMS{
    if( [MFMessageComposeViewController canSendText] ) {
        NSString *description = self.shareSMSString;
        MFMessageComposeViewController * controller = [[MFMessageComposeViewController alloc]init];
        controller.body = description;
        controller.messageComposeDelegate = self;
        [[NSNotificationCenter defaultCenter] postNotificationName:PHNotificationSendShareCodeObserver object:nil];
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        [self alertTitle:@"提示" message:@"您的设备不支持短信功能。" actionTitle:@"好的"];
    }
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    typeof(self) __weak weakSelf = self;
    [controller dismissViewControllerAnimated:YES completion:^{
        [weakSelf dismiss];
    }];
}

- (void)alertTitle:(NSString *)title message:(NSString *)message actionTitle:(NSString *)actionTitle{
    typeof(self) __weak weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf dismiss];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)sendLinkContent:(NSString *)title withDescription:(NSString *)description withImage:(NSString *)image withUrl:(NSString *)url scene:(int)scene{
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = title;
    message.description = description;
    [message setThumbImage:[self imagesNamedFromCustomBundle:image]];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = url;
    
    message.mediaObject = ext;
    message.mediaTagName = @"WECHAT_TAG_JUMP_SHOWRANK";
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    [WXApi sendReq:req];
}

- (UIImage *)imagesNamedFromCustomBundle:(NSString *)imgName {
    NSString *bundlePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"HFaxSDK.bundle"];
    if (bundlePath) {
        NSString *img_path = [bundlePath stringByAppendingPathComponent:imgName];
        return [UIImage imageWithContentsOfFile:img_path];
    } else {
        return nil;
    }
}

@end
