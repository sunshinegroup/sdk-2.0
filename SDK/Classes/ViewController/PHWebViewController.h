//
//  PHWebViewController.h
//  Pods
//
//  Created by 邹彦军 on 16/6/27.
//
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "HJSService.h"
#import "WKWebViewJavascriptBridge.h"


@interface PHWebViewController : UIViewController<WKNavigationDelegate>
@property (nonatomic, copy) NSURL *webUrl;
@property (nonatomic, assign) BOOL showProgressBar;
@property (nonatomic, copy, readonly) WVJBResponseCallback popToEntryCallback;
@property (nonatomic, copy, readonly) void(^statusHander)(HJSStatus status,NSDictionary *info);
@property (nonatomic, copy, readonly) WVJBResponseCallback lastPresentCallback;
@property (nonatomic, weak, readonly) UIViewController *fromController;
@property (nonatomic, strong, readonly) WKWebViewJavascriptBridge *bridge;
@property (nonatomic, strong, readonly) WKWebViewConfiguration* baseWebViewConfig;

- (instancetype)initWithParameterData:(NSDictionary *)parameterData url:(NSURL *)url withStatusHander:(void(^)(HJSStatus status,NSDictionary *info))statusHander lastPushCallback:(WVJBResponseCallback)lastPushCallback popToEntryCallback:(WVJBResponseCallback)popToEntryCallback lastPresentCallback:(WVJBResponseCallback)lastPresentCallback isEntry:(BOOL)isEntry entryController:(PHWebViewController *)entryController fromController:(UIViewController *)fromController withWebViewConfiguration:(WKWebViewConfiguration*)configuration;

- (BOOL)isConnected;
- (void)addStatusBarView;
- (WKWebView *)obtainWebView;
- (void)pushToErrorController;
+ (instancetype)sharedInstance;
+ (void)setStatusBarBackgroungViewHidden:(BOOL)hidden;
+ (void)setStatusBarBackgroundColor:(NSUInteger)statusBarBackgroundColor;
- (void)setupRegisterHandler;

@end
