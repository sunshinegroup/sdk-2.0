//
//  PHWebViewController.m
//  Pods
//
//  Created by 邹彦军 on 16/6/27.
//
//
#import "HJSToolClass.h"
#import "MBProgressHUD.h"
#import "HJSReachability.h"
#import "PHWebViewController.h"
#import "PHShareViewController.h"
#import "HJSErrorPageViewController.h"

static const NSInteger kAlertPromptTag = 666;
static const NSInteger kAlertCallPhoneTag = 777;
static BOOL statusBarBackgroundViewHidden = NO;
static NSUInteger kStatusBarBackgroundColor = 0xF13D3D;
static NSString * const kAlertPromptCall = @"呼叫";
static NSString * const kAlertPromptTitle = @"提示";
static NSString * const kAlertPromptCancel = @"取消";
static NSString * const kAlertPromptConfirm = @"确定";
static NSString * const kAlertPromptIsCallPhone = @"是否拨打客服电话";
static NSString * const kAlertPromptNonsupportCallPhone = @"当前设备不支持打电话";

@interface PHWebViewController ()<UIAlertViewDelegate>

@property (nonatomic, assign) BOOL isEntry;
@property (nonatomic, assign) BOOL isPopping;
@property (nonatomic, strong) UIView *statusView;
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, copy) NSDictionary *dataInfo;
@property (nonatomic, copy) NSString * phoneNumber;
@property (nonatomic, copy) NSString * unionPayReturnActionUrl;   // 隐藏自定义导航栏
@property (nonatomic, copy) NSString * unionPayRedirectActionUrl; // 显示自定义导航栏
@property (nonatomic, strong) UIView *customNavigationBar;
@property (nonatomic, copy) NSDictionary *alertPromptData;
@property (nonatomic, assign) NSUInteger numberOfRequests;
@property (nonatomic, weak, readwrite) UIViewController *fromController;
@property (nonatomic, strong, readwrite) WKWebViewJavascriptBridge *bridge;
@property (nonatomic, strong) UILabel *customNavigationBarTitle;
@property (nonatomic, weak) PHWebViewController *entryController;
@property (nonatomic, copy) WVJBResponseCallback lastPushCallback;
@property (nonatomic, copy, readwrite) WVJBResponseCallback popToEntryCallback;
@property (nonatomic, copy, readwrite) WVJBResponseCallback lastPresentCallback;
@property (nonatomic, strong,) UIButton *customNavigationBarBackButton;
@property (nonatomic, copy) WVJBResponseCallback alertPromptConfirmTappedCallback;
@property (nonatomic, copy, readwrite) void(^statusHander)(HJSStatus status,NSDictionary *info);
@property (nonatomic, strong) HJSReachability *reachability;
@property (nonatomic, strong, readwrite) WKWebViewConfiguration* baseWebViewConfig;
@property (nonatomic,strong)UIProgressView *progressView;

@end

@implementation PHWebViewController

+ (instancetype)sharedInstance {
    static PHWebViewController *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [PHWebViewController new];
        instance.baseWebViewConfig = [WKWebViewConfiguration new];
        instance.baseWebViewConfig.processPool = [[WKProcessPool alloc] init];
    });
    return instance;
}

- (instancetype)initWithParameterData:(NSDictionary *)parameterData url:(NSURL *)url withStatusHander:(void(^)(HJSStatus status,NSDictionary *info))statusHander lastPushCallback:(WVJBResponseCallback)lastPushCallback popToEntryCallback:(WVJBResponseCallback)popToEntryCallback lastPresentCallback:(WVJBResponseCallback)lastPresentCallback isEntry:(BOOL)isEntry entryController:(PHWebViewController *)entryController fromController:(UIViewController *)fromController withWebViewConfiguration:(WKWebViewConfiguration*)configuration{
    
    if (self = [super init]) {
        _isEntry = isEntry;
        _webUrl = [url copy];
        _dataInfo = [parameterData copy];
        _fromController = fromController;
        _entryController = entryController;
        _statusHander = [statusHander copy];
        _lastPushCallback = [lastPushCallback copy];
        _popToEntryCallback = [popToEntryCallback copy];
        _lastPresentCallback = [lastPresentCallback copy];
        _baseWebViewConfig = configuration;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear: animated];
    
    if (self.isEntry && self.isPopping) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    
    self.reachability = [HJSReachability reachabilityForInternetConnection];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.webUrl]];
    
    self.bridge = [WKWebViewJavascriptBridge bridgeForWebView:self.webView];
    [self.bridge setWebViewDelegate:self];
    
#ifdef DEBUG
    [WKWebViewJavascriptBridge enableLogging];
#endif
    [self setupRegisterHandler];
}

- (void)setupUI {
    if(self.baseWebViewConfig){
        self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:self.baseWebViewConfig];
    }else{
        self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:[PHWebViewController sharedInstance].baseWebViewConfig];
    }
    [self.view addSubview:self.webView];
    self.customNavigationBar = [UIView new];
    self.customNavigationBar.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, 44);
    self.customNavigationBar.hidden = YES;
    self.customNavigationBar.backgroundColor = UIColorFromRGB(0xF13D3D);
    [self.view addSubview:self.customNavigationBar];
    
    self.customNavigationBarBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.customNavigationBarBackButton.frame = CGRectMake(0, 0, 44, 44);
    [self.customNavigationBarBackButton setImage:[self imagesNamedFromCustomBundle:@"icon_backButton@2x"] forState:UIControlStateNormal];
    [self.customNavigationBarBackButton addTarget:self action:@selector(customNavigationBarBackButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.customNavigationBar addSubview:self.customNavigationBarBackButton];
    
    self.customNavigationBarTitle = [UILabel new];
    self.customNavigationBarTitle.frame = self.customNavigationBar.bounds;
    self.customNavigationBarTitle.text = @"绑定银行卡";
    self.customNavigationBarTitle.font = [UIFont fontWithName:@ "Helvetica-Bold"  size:(18.0)];
    self.customNavigationBarTitle.textAlignment =  UITextAlignmentCenter;
    self.customNavigationBarTitle.textColor = [UIColor whiteColor];
    [self.customNavigationBar addSubview:self.customNavigationBarTitle];
    
    if (!statusBarBackgroundViewHidden) {
        [self addStatusBarView];
    }
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
    self.progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(0,0,self.view.bounds.size.width,3)];
    self.progressView.tintColor =[UIColor greenColor];
    self.progressView.trackTintColor = [UIColor whiteColor];
    [self.view addSubview:self.progressView];
    self.showProgressBar = self.showProgressBar ? :(self.progressView.hidden = YES);
}

- (BOOL)isConnected {
    return ([self.reachability currentReachabilityStatus]!= NotReachable);
}

- (void)setupRegisterHandler {
    typeof(self) __weak weakSelf = self;
    
    if (![self.reachability currentReachabilityStatus]) {
        [self pushToErrorController];
    }
    [self.bridge registerHandler:@"push" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf pushViewControllerData:data pushCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"present" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf presentViewControllerData:data presentCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"pop" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf popViewControllerData:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"popToEntry" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf popToEntryControllerData:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"close" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf closeViewControllerData:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"exit" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf exitTheSDKData:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"alert" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf alertViewController:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"showLoadingIndicator" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf showLoadingIndicator:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"hideLoadingIndicator" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf hideLoadingIndicator:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"share" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf showShareView:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"purchaseSuccess" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf purchaseSuccess:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"purchaseError" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf purchaseError:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"purchaseInProgress" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf purchaseInProgress:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"callPhone" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf callPhone:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"reloadWebView" handler:^(id data, WVJBResponseCallback responseCallback) {
        [weakSelf reloadWebView:data responseCallback:responseCallback];
    }];
    
    [self.bridge registerHandler:@"passParameter" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self acceptParametersData:data callback:responseCallback];
    }];
}

- (void)pushViewControllerData:(id)data pushCallback:(WVJBResponseCallback)callback {
    
    NSString *targetUrlString = [data valueForKey:@"url"];
    NSURL * targetUrl = [NSURL URLWithString:[targetUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
#ifdef UITEST
    NSString *contentStr = [targetUrlString substringFromIndex:8];
    targetUrl = [NSURL URLWithString:[[NSBundle mainBundle]pathForResource:contentStr ofType:nil]];
#endif
    
    PHWebViewController * entryController = self.entryController ? : self;
    WVJBResponseCallback popToEntryCallback = self.popToEntryCallback ? : callback;
    
    PHWebViewController * w = [[self.class alloc] initWithParameterData:nil url:targetUrl withStatusHander:self.statusHander lastPushCallback:callback popToEntryCallback:popToEntryCallback lastPresentCallback:self.lastPresentCallback isEntry:NO entryController:entryController fromController:self.fromController withWebViewConfiguration:self.baseWebViewConfig];
    
    w.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:w animated:YES];
}

- (void)presentViewControllerData:(id)data presentCallback:(WVJBResponseCallback)callback {
    
    NSString *targetUrlString = [data valueForKey:@"url"];
    NSURL * targetUrl = [NSURL URLWithString:[targetUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
#ifdef UITEST
    NSString *contentStr = [targetUrlString substringFromIndex:8];
    targetUrl = [NSURL URLWithString:[[NSBundle mainBundle]pathForResource:contentStr ofType:nil]];
#endif
    
    PHWebViewController * entryController = self.entryController ? : self;
    WVJBResponseCallback popToEntryCallback = self.popToEntryCallback ? : callback;
    
    PHWebViewController * w = [[self.class alloc] initWithParameterData:nil url:targetUrl withStatusHander:self.statusHander lastPushCallback:nil popToEntryCallback:popToEntryCallback lastPresentCallback:callback isEntry:NO entryController:entryController fromController:self.fromController withWebViewConfiguration:self.baseWebViewConfig];
    
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:w] animated:YES completion:nil];
}

- (void)popViewControllerData:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    NSString *jsonData =[self dictionaryToJson:data];
    
    self.isPopping = YES;
    [self.navigationController popViewControllerAnimated:YES];
    
    if (responseCallback) {
        responseCallback(jsonData);
    }
    if (self.lastPushCallback) {
        self.lastPushCallback(jsonData);
    }
}

- (void)popToEntryControllerData:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    NSString *jsonData =[self dictionaryToJson:data];
    typeof (self) __weak weakSelf = self;
    
    if (self.entryController.presentedViewController) {
        [self.entryController dismissViewControllerAnimated:YES completion:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.entryController.navigationController popToViewController:weakSelf.entryController animated:NO];
        });
    } else {
        [self.navigationController popToViewController:self.entryController animated:YES];
    }
    
    if (responseCallback) {
        responseCallback(jsonData);
    }
    if (self.popToEntryCallback) {
        self.popToEntryCallback(jsonData);
    }
}

- (void)closeViewControllerData:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    NSString *jsonData =[self dictionaryToJson:data];
    typeof (self) __weak weakSelf = self;
    
    if (!self.lastPresentCallback) {
        [self.fromController.navigationController popToViewController:self.fromController animated:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.fromController.navigationController setNavigationBarHidden:NO animated:YES];
        });
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            if (responseCallback) {
                responseCallback(jsonData);
            }
            if (weakSelf.lastPresentCallback) {
                weakSelf.lastPresentCallback(jsonData);
            }
        }];
    }
}

- (void)exitTheSDKData:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    typeof (self) __weak weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.fromController.navigationController setNavigationBarHidden:NO animated:YES];
    });
    
    if (self.entryController.presentedViewController) {
        [self.entryController dismissViewControllerAnimated:YES completion:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.fromController.navigationController popToViewController:weakSelf.fromController animated:NO];
        });
    } else {
        if (self.presentedViewController) {
            [self dismissViewControllerAnimated:YES completion:^{
                [weakSelf.fromController.navigationController popToViewController:self.fromController animated:YES];
            }];
        }else {
            [self.fromController.navigationController popToViewController:self.fromController animated:YES];
        }
        
    }
}

- (void)alertViewController:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    typeof (self) __weak weakSelf = self;
    self.alertPromptConfirmTappedCallback = responseCallback;
    self.alertPromptData = data;
    NSString *alertPromptTitle = [data valueForKey:@"title"];
    
    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertPromptTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:kAlertPromptConfirm style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (weakSelf.alertPromptConfirmTappedCallback) {
                weakSelf.alertPromptConfirmTappedCallback(weakSelf.alertPromptData);
            }
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:kAlertPromptCancel style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            self.alertPromptConfirmTappedCallback = nil;
            self.alertPromptData = nil;
        }];
        
        [alertController addAction:confirmAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        [self showAlertTitle:alertPromptTitle message:nil delegate:self leftButtonContent:kAlertPromptConfirm rightButtonContent:kAlertPromptCancel alertTag:kAlertPromptTag];
    }
}

- (void)showLoadingIndicator:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    NSTimeInterval timeout = [[data valueForKey:@"timeout"] doubleValue];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.webView];
    if (!hud) {
        hud = [MBProgressHUD showHUDAddedTo:self.webView animated:YES];
        
        if (timeout > 0) {
            [hud hide:YES afterDelay:timeout];
            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timeout * NSEC_PER_SEC));
            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                if(responseCallback){
                    responseCallback(data);
                }
            });
        } else if (responseCallback) {
            responseCallback(data);
        }
    }
}

- (void)hideLoadingIndicator:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.webView];
    [hud hide:YES];
    
    if (responseCallback) {
        responseCallback(data);
    }
}

- (void)showShareView:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    NSString *shareUrl     = [data valueForKey:@"url"];
    NSString *shareTitle   = [data valueForKey:@"title"];
    NSString *shareFriend  = [data valueForKey:@"friend"];
    NSString *shareContent = [data valueForKey:@"content"];
    NSString *shareSMS     = [data valueForKey:@"smscontent"];
    
    PHShareViewController *shareVC = [[PHShareViewController alloc] initWithShareTitleString:shareTitle shareContentString:shareContent shareSMSString:shareSMS shareFriendString:shareFriend shareUrlString:shareUrl dismissHandler:^(PHShareViewController *controller) {
        [controller willMoveToParentViewController:nil];
        [controller removeFromParentViewController];
        [controller.view removeFromSuperview];
    }];
    
    [self addChildViewController:shareVC];
    [self.view addSubview:shareVC.view];
    shareVC.view.frame = self.view.bounds;
    [shareVC didMoveToParentViewController:self];
}

- (void)purchaseSuccess:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    if (responseCallback) {
        responseCallback(data);
    }
    if (self.statusHander) {
        self.statusHander(HJSSuccess,[data valueForKey:@"result"]);
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)purchaseError:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    if (responseCallback) {
        responseCallback(data);
    }
    if (self.statusHander) {
        self.statusHander(HJSError,[data valueForKey:@"result"]);
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)purchaseInProgress:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    if (responseCallback) {
        responseCallback(data);
    }
    if (self.statusHander) {
        self.statusHander(HJSUnknown,[data valueForKey:@"result"]);
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)reloadWebView:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    NSString *urlString = [data valueForKey:@"url"];
    NSURL *accessUrl = [NSURL URLWithString:urlString];
    
#ifdef UITEST
    urlString = [urlString substringFromIndex:8];
    accessUrl = [NSURL URLWithString:[[NSBundle mainBundle]pathForResource:urlString ofType:nil]];
#endif
    
    if (urlString.length) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
        [self.webView loadRequest:[NSURLRequest requestWithURL:accessUrl]];
    } else {
        [self.webView reload];
    }
}

- (void)callPhone:(id)data responseCallback:(WVJBResponseCallback)responseCallback {
    
    data = [self sanitizeData:data];
    
    self.phoneNumber = [data valueForKey:@"phoneNumber"];
    
    if (NSClassFromString(@"UIAlertController")) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertPromptTitle message:kAlertPromptIsCallPhone preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:kAlertPromptCall style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.phoneNumber]]];
            }else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:kAlertPromptNonsupportCallPhone preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:kAlertPromptConfirm style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:kAlertPromptCancel style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else {
        [self showAlertTitle:kAlertPromptTitle message:kAlertPromptIsCallPhone delegate:self leftButtonContent:kAlertPromptCall rightButtonContent:kAlertPromptCancel alertTag:kAlertCallPhoneTag];
    }
}

- (NSDictionary *)sanitizeData:(id)data{
    
    if ([data isKindOfClass:[NSDictionary class]]) {
        return data;
    }else if (data == nil){
        return @{};
    }else{
#ifdef DEBUG
        NSLog(@"data类型异常:\n%@",data);
#endif
        return @{};
    }
}

- (NSString *)dictionaryToJson:(NSDictionary *)dict{
    
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:kNilOptions error:&parseError];
    if (parseError) {
#ifdef DEBUG
        NSLog(@"json序列化异常:\n%@",parseError);
#endif
        return @"{}";
    }
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (void)showAlertTitle:(NSString *)title message:(NSString *)message delegate:(nullable id)delegate leftButtonContent:(NSString *)leftButtonContent rightButtonContent:(NSString *)rightButtonContent alertTag:(NSInteger)alertTag {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:leftButtonContent otherButtonTitles:rightButtonContent, nil];
    alertView.tag = alertTag;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{//低于iOS8.0的操作系统会进入的代理方法
    
    if (buttonIndex != 0) {
        self.alertPromptConfirmTappedCallback = nil;
        self.alertPromptData = nil;
        return;
    };
    
    if (alertView.tag == kAlertPromptTag && buttonIndex == 0) {//点击alert的'确定'按钮
        if (self.alertPromptConfirmTappedCallback) {
            self.alertPromptConfirmTappedCallback(self.alertPromptData);
        }
    }else if (alertView.tag == kAlertCallPhoneTag && buttonIndex == 0){//点击alert的'呼叫'按钮
        
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.phoneNumber]]];
        }else{
            [self showAlertTitle:nil message:kAlertPromptNonsupportCallPhone delegate:nil leftButtonContent:kAlertPromptConfirm rightButtonContent:nil alertTag:0];
        }
    }
}

- (void)acceptParametersData:(id)data callback:(WVJBResponseCallback)callback {
    self.unionPayReturnActionUrl = [data valueForKey:@"unionPayReturnActionUrl"];
    self.unionPayRedirectActionUrl = [data valueForKey:@"unionPayRedirectActionUrl"];
}

- (void)updateLoadingHUD {
    MBProgressHUD *hud = [MBProgressHUD HUDForView:[self obtainWebView]];
    
    if (self.numberOfRequests > 0) {
        if (!hud) {
            [MBProgressHUD showHUDAddedTo:[self obtainWebView] animated:YES];
        }
    } else {
        [hud hide:YES];
    }
}

+ (void)setStatusBarBackgroundColor:(NSUInteger)statusBarBackgroundColor {
    kStatusBarBackgroundColor = statusBarBackgroundColor;
}

+ (void)setStatusBarBackgroungViewHidden:(BOOL)hidden {
    statusBarBackgroundViewHidden = hidden;
}

- (void)addStatusBarView {
    
    if (self.statusView == nil) {
        self.statusView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
        self.statusView.backgroundColor = UIColorFromRGB(kStatusBarBackgroundColor);
        [self.view addSubview:self.statusView];
    }
}

- (UIImage *)imagesNamedFromCustomBundle:(NSString *)imgName {
    NSString *bundlePath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:@"HFaxSDK.bundle"];
    if (bundlePath) {
        NSString *img_path = [bundlePath stringByAppendingPathComponent:imgName];
        return [UIImage imageWithContentsOfFile:img_path];
    } else {
        return nil;
    }
}

- (void)pushToErrorController {
    typeof(self) __weak weakSelf = self;
    
    HJSErrorPageViewController *errorPageVC = [HJSErrorPageViewController new];
    [errorPageVC setSuccessHandler:^(HJSErrorPageViewController *controller) {
        [weakSelf exitTheSDKData:nil responseCallback:nil];
    }];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:errorPageVC] animated:NO completion:nil];
}

-(void)customNavigationBarBackButtonClick {
    
    if (![self.reachability currentReachabilityStatus]) {
        [self pushToErrorController];
        return;
    }
    
    NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:self.unionPayReturnActionUrl]];
    [self.webView loadRequest:requset];
    
    self.customNavigationBar.hidden = YES;
    self.webView.frame = self.view.bounds;
}

- (WKWebView *)obtainWebView {
    return self.webView;
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    self.numberOfRequests ++;
    [self updateLoadingHUD];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    self.numberOfRequests --;
    [self updateLoadingHUD];
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    
    self.numberOfRequests = 0;
    [self updateLoadingHUD];
    
    // 隐藏自定义导航栏
    if (!self.customNavigationBar.hidden) {
        self.customNavigationBar.hidden = YES;
        self.webView.frame = self.view.bounds;
    }
    
    if (![self.reachability currentReachabilityStatus]) {
        [self pushToErrorController];
        return;
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    
    // 显示自定义导航栏
    if ([[navigationResponse.response.URL absoluteString] isEqualToString:self.unionPayRedirectActionUrl]) {
        self.customNavigationBar.hidden = NO;
        self.webView.frame = CGRectMake(0, 44, [UIScreen mainScreen].bounds.size.width, self.view.bounds.size.height - 44);
    }
    decisionHandler(WKNavigationResponsePolicyAllow);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (object ==_webView) {
            [self.progressView setAlpha:1.0f];
            [self.progressView setProgress:self.webView.estimatedProgress animated:YES];
            typeof(self) __weak weakSelf = self;
            if(self.webView.estimatedProgress >=1.0f) {
                [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [weakSelf.progressView setAlpha:0.0f];
                }completion:^(BOOL finished){
                    [weakSelf.progressView setProgress:0.0f animated:NO];
                }];
            }
        }else{
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }
}
- (void)dealloc
{
    if([self isViewLoaded]){
        [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    }
    [self.webView setNavigationDelegate:nil];
    
}
@end
