#
# Be sure to run `pod lib lint SDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "HFaxSDK"
    s.version          = "1.20.3"
    s.summary          = "惠金所iOS SDK"

    s.description      = <<-DESC
        惠金所理财平台iOS SDK，支持原生交互
    DESC

    s.homepage         = "https://bitbucket.org/sunshinegroup/sdk-2.0"
    s.license          = 'MIT'
    s.author           = { "Philip" => "philip.cn@gmail.com" }
    s.source           = { :git => "https://bitbucket.org/sunshinegroup/sdk-2.0.git", :branch => 'master' }

    s.ios.deployment_target = '5.0'

    s.source_files = 'SDK/Classes/**/*'
  
    s.resource_bundles = {
        'SDK' => ['SDK/Assets/*.png']
    }

    s.public_header_files = 'Pod/Classes/**/*.h'
    s.libraries  = 'sqlite3','z','C++'
    s.frameworks = 'UIKit', 'Security', 'SystemConfiguration', 'CoreGraphics','CoreTelephony'
    s.dependency 'WebViewJavascriptBridge', '~> 6.0.2'
    s.dependency 'libWeChatSDK', '~> 1.7.5'
    s.xcconfig = {
      'GCC_PREPROCESSOR_DEFINITIONS' => 'SANDBOX'
    }
end
